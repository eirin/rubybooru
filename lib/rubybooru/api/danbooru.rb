module RubyBooru::Api
    class Danbooru < Booru
        def count_posts tags = nil
            self.request("/counts/posts.json", {:tags => tags})
        end

        def list_artist_comments **params
            params = params.slice(:text_matches, :post_id,
                                  :post_tags_match, :original_present,
                                  :translated_present).transform_keys {|k|
                                      "search[#{k}]" }
            self.request("/artist_commentaries.json", params)
        end

        def list_comments **params
            not_search = params.slice(:page, :group_by, :limit)
            params     = params.slice(:text_matches, :post_id,
                                      :post_tags_match, :original_present,
                                      :translated_present).transform_keys {|k|
                                          "search[#{k}]" }
            self.request("/comments.json", params.merge(not_search))
        end

        def list_notes **params
            params = params.slice(:body_matches, :post_id,
                                  :post_tags_match, :creator_name,
                                  :creator_id, :is_active).transform_keys {|k|
                                      "search[#{k}]" }
            self.request("/notes.json", params)
        end

        def list_post **params
            params = params.slice(:limit, :page, :tags, :md5, :random, :raw)

            self.request("/posts.json", params).map {|post|
                RubyBooru::Model::Post.new(
                    json: post, booru_connection: self) if !post['is_banned']
            }
        end

        def show_post id = nil, md5: nil, json: false
            if id
                return self.request("/posts/#{id}.json", {:id => id}) if json
                RubyBooru::Model::Post.new(
                    json: self.request("/posts/#{id}.json", {:id => id}),
                    booru_connection: self)
            elsif md5
                return self.request("/posts.json", {:md5 => md5}) if json
                RubyBooru::Model::Post.new(
                    json: self.request("/posts/#{id}.json", {:id => id}),
                    booru_connection: self)
            else
                raise BooruError.new "Specify an ID or MD5."
            end
        end
    end
end