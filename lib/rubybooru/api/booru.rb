module RubyBooru::Api
    class Booru
        attr_reader :site, :name

        def initialize(name: nil, site: nil)
            if !name.nil?
                self.name = name
            elsif !site.nil?
                self.site = site
            else
                raise BooruError.new "No endpoint provided. " \
                    "Provide site name or url."
            end

            @connection = RubyBooru::Net::HTTP.session site: @site
        end

        def name=(name)
            if VALID_SITES.has_key?(name)
                @name = name
                @site = VALID_SITES[name][:url]
            else
                raise BooruError.new "Invalid site name. " \
                    "Valid site names: #{VALID_SITES.keys.join(" ")}"
            end
        end

        def site=(site)
            raise BooruError.new "Please use HTTP or HTTPS." \
                                 unless site =~ /^(?:http|https):\/\//
            raise BooruError.new "Invalid url: #{site}." \
                                 unless site =~ URI.regexp
            @site = site
        end

        def request(path, params = {}, method = :get)
            request = @connection.build_request(method) do |req|
                req.url(path)
                req.params = params
                req.headers['content-type'] = nil if method != :get
            end

            $lg.debug("Requesting #{request.path}; Params: #{request.params}")
            @connection.builder.build_response(@connection, request).body
        end
    end
end