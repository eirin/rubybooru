require 'sequel'
require 'sequel/extensions/migration'

module RubyBooru::Database
    attr_reader :connection

    class << self
        def connect
            return false if connected?
            db_config = $conf['database']
            db_logger = Logger.new(STDOUT)
            db_logger.level = $lg.level == Logger::DEBUG ? Logger::DEBUG :
                                                           Logger::WARN
            db_config['loggers'] = [$lg.loggers.last, db_logger]

            begin
                $lg.debug("Connecting to database: #{db_config['database']}")
                @@connection = Sequel.connect(db_config)
            rescue Exception => e
                $lg.fatal("A fatal error has ocurred. Full error message:")
                puts e.backtrace
                puts e.message
                exit 101
            else
                $lg.info("Running migration check, do not mind errors below")
                Sequel::Migrator.run(@@connection, File.expand_path('./database/migrate', __dir__))

                Dir[File.join(__dir__, 'models', '*.rb')].each do |file|
                    require file
                end

                $lg.debug("Successfully connected to database.")
            end
        end

        def connected?;defined?(@@connection) && !@@connection.nil?;end

        def disconnect
            return false if !connected?
            db_config = $conf['database']

            $lg.debug("Disconnecting from database: " \
                             "#{db_config['database']}")
            @@connection.disconnect
            @@connection = nil
            $lg.debug("Successfully disconnected from database: " \
                             "#{db_config['database']}")
        end

        def reconnect
            @@connection = nil
            connect
        end

        def create
            db_config = $conf['database']
            db_config['test'] = false

            begin
                $lg.debug("Creating database: #{db_config['database']}")
                raise RubyBooru::Conf::DatabaseConfigurationError.new("Database not " \
                    "suported for current operation.") unless [
                    'mysql',
                    'mysql2',
                    'postgresql',
                    'sqlite'
                ].include?(db_config['adapter'])
                if db_config['adapter'] == 'sqlite'
                    connect
                    disconnect
                else
                    Sequel.connect(db_config.select{|k,_|k!="database"}) do |db|
                        db.execute "CREATE DATABASE IF NOT EXISTS #{db_config['database']}"
                    end
                end
            rescue RubyBooru::Conf::DatabaseConfigurationError => e
                $lg.logger.fatal(e.message)
                exit 103
            rescue Exception => e
                $lg.fatal("A fatal error has ocurred. Full error message:")
                puts e.backtrace
                puts e.message
                exit 104
            else
                $lg.info("Successfully created database.")
            end
        end

        def connection
            self.connect if !connected?
            @@connection
        end

        def drop_database
            db_config = $conf['database']
            db_config['test'] = false

            disconnect

            begin
                $lg.info("Deleting database: #{db_config['database']}")
                if ['mysql', 'mysql2', 'postgresql'].include?(db_config['adapter'])
                    Sequel.connect(db_config.select{|k,_|k!="database"}) do |db|
                        db.execute "DROP DATABASE IF EXISTS #{db_config['database']}"
                    end
                elsif File.file?(db_config['database'])
                    File.delete(db_config['database'])
                else
                    raise DatabaseNotFoundError.new
                end
            rescue DatabaseNotFoundError => e
                $lg.fatal(e.message)
                exit 103
            rescue Exception => e
                $lg.fatal("A fatal error has ocurred. Full error message:")
                puts e.backtrace
                puts e.message
                exit 104
            else
                $lg.info("Successfully dropped database.")
            end
        end
    end

    class DatabaseNotFoundError < StandardError
        def initialize(msg = "Database was not found. Please check" \
            "the database section in your configuration.")
            super(msg)
        end
    end
end