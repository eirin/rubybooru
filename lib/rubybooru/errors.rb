require 'faraday_middleware'

module RubyBooru
    module Conf
        class DatabaseConfigurationError < StandardError
            def initialize(msg = "Database configured wrongly. Please check" \
                                "the database section in your configuration.")
                super(msg)
            end
        end
    end

    module Api
        class BooruError < StandardError
            def initialize(msg = "A lib specific error has ocurred.")
                super(msg)
            end
        end
    end

    module Net
        class BooruNetworkError < Api::BooruError
            def initialize(msg = "Network error has ocurred.",
                           status_code:)
                super(msg + " #{status_code} - " \
                      "#{STATUS_CODES[status_code].values.join(": ")}")
            end
        end
    
        class UserThrottledError < BooruNetworkError
            def initialize(msg = STATUS_CODES[429][:desc])
                super(msg, 429)
            end
        end
    end
end