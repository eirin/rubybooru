require 'sequel'

module RubyBooru::Model
    module Rec
        Record = Class.new(Sequel::Model) do
            def self.delete_all
                self.dataset.delete
            end
        end

        Record.def_Model(self)
        Record.plugin(:prepared_statements)
    end
end