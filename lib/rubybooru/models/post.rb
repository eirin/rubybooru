require 'digest'
require 'fileutils'
require 'json'

module RubyBooru::Model
    class Post
        attr_accessor :id
        attr_reader :json, :media, :artist_comments, :comments, :notes, :paths

        def initialize id: nil, json: nil, booru_connection: nil,
                       store_path: $conf["store"]
            @id         = id
            @bc         = booru_connection
            @store_path = store_path
            self.json = json
        end

        [:artist_comments, :comments, :notes].each do |func|
            define_method("fetch_#{func}") do |force = false|
                return false if !force && !self.send("has_#{func}?")
                check_booru_connection
                instance_variable_set(:"@#{func}",
                    @bc.send("list_#{func}", post_id: self.id)) \
                    if !instance_variable_defined?(:"@#{func}")
                instance_variable_get(:"@#{func}")
            end

            define_method("write_#{func}") do |force = false|
                return false if !self.send("fetch_#{func}", force) \
                    if !instance_variable_defined?(:"@#{func}")
                write_store(@paths[func],
                    JSON.pretty_generate(instance_variable_get(:"@#{func}")))
            end
        end

        def check_media download = true
            fetch_post if @json.nil?

            if @ugoira
                $lg.debug("Not checking MD5 for #{@id}, because it is an" \
                          "ugoira")
                return true
            end

            $lg.debug("Checking media of #{@id}...")
            md5 = Digest::MD5.new

            begin
                File.open(@paths[:media], "rb") do |m|
                    m.each { |line| md5 << line }
                end

                return true if md5 == @json['md5']
                $lg.error("MD5 mismatch for #{@paths[:media]}!")

                if download
                    $lg.info("Redownloading media for #{@id}...")
                    write_media
                end
            rescue Errno::ENOENT
                $lg.debug("File not found while checking media md5:" \
                    "#{@paths[:media]}. Download media first.")
            end

            return false
        end

        def fetch_post
            check_id

            begin
                self.json = JSON.parse(File.read(File.join(@store_path,@paths[:json])))
                $lg.debug("Loaded json from disk")
            rescue Errno::ENOENT
                check_booru_connection
                self.json = @bc.show_post(@id, json: true)
                $lg.debug("Loaded json from #{@bc.name}")
            end
        end

        def has_artist_comments?
            fetch_post if @json.nil?
            return !(@json['tag_string_meta'] !~ /commentary/)
        end

        def has_comments?
            fetch_post if @json.nil?
            return !(@json['last_comment_bumped_at'] ||
                     @json['last_commented_at']).nil?
        end

        def has_notes?
            fetch_post if @json.nil?
            return !@json['last_noted_at'].nil?
        end

        def write_all force = false
            write_json
            write_media force

            write_artist_comments force
            write_comments        force
            write_notes           force

            return true
        end

        def write_json
            fetch_post if @json.nil?
            write_store @paths[:json], JSON.pretty_generate(@json)
        end

        def write_media force = false
            @downloaded_media = File.file?(@paths[:media]) \
                if !instance_variable_defined?("@downloaded_media")

            return false if !force && @downloaded_media
            fetch_post if @json.nil?

            url = @ugoira ? @json["large_file_url"] : @json["file_url"]

            File.open(@paths[:media], "wb") do |f|
                RubyBooru::Net::HTTP.stream url do |chunk, recv|
                    f.write(chunk)
                end
            end

            check_media
        end

        def booru_connection=(bc);@bc = bc;end
        def booru_connection;     @bc;     end

        def json=(json)
            @json = json

            if !json.nil?
                @id     = @json['id']
                @ugoira = @json['tag_string_meta'] =~ /ugoira/

                @paths = {
                    :json            => File.join('json',            "#{@id}.json"),
                    :artist_comments => File.join('artist_comments', "#{@id}.json"),
                    :comments        => File.join('comments',        "#{@id}.json"),
                    :notes           => File.join('notes',           "#{@id}.json"),
                    :media           => File.join(@store_path,       'media',
                        "#{@id}.#{ @ugoira ?
                            @json['large_file_url'].split(".").last    :
                            @json['file_ext']}")
                }
            end
        end

        private
            def check_booru_connection
                raise RubyBooru::Api::BooruError.new("You must specify a booru object first.") \
                    if @bc.nil?
            end
        
            def check_id
                raise RubyBooru::Api::BooruError.new("You must specify an ID first.") if @id.nil?
            end

            def write_store name, content
                File.write(File.join(@store_path, name), content)
            end
    end
end