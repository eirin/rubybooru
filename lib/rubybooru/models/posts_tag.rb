require 'rubybooru/models/record'

module RubyBooru::Model
    class PostsTag < Rec::Record
        unrestrict_primary_key
    end
end