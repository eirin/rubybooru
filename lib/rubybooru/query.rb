module RubyBooru
    class Query
        attr_reader :booru
        attr_accessor :limit, :tags, :random

        CACHE_DIR = File.join($conf["store"], 'cache')

        def initialize limit: 20, random: false, booru: 'safebooru', tags: nil
            @limit  = limit
            @tags   = tags
            @random = random

            self.booru = booru
        end

        def execute &block
            pages    = [@limit / 200, 1000].min
            last     = @limit % 200
            results  = Array.new if !block_given?
            continue = true

            pages.times do |i|
                $lg.info("Fetching page #{i+1}/#{pages+1}")

                result = @booru.list_post(tags: @tags, limit: 200,
                                          random: @random, page: i+1).compact
                if result.empty?
                    $lg.warn("Stopping because there is no more posts")
                    continue = false
                    break
                end

                block_given? ? yield(result, i+1) : results += result
            end

            if continue
                $lg.info("Fetching page #{pages+1}/#{pages+1}")
                result = @booru.list_post(tags: @tags, limit: last,
                                          random: @random,
                                          page: pages+1).compact
                block_given? ? yield(result, pages+1) : results += result
            end
        end
        
        def booru=(booru)
            @booru = RubyBooru::Api.get_client booru
        end

        def limit=(limit)
            if limit <= 0
                raise RangeError, 'Limit value cannot be negative'
            end

            @limit = limit
        end
    end
end