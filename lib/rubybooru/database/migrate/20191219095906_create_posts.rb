Sequel.migration do
    up do
        create_table :posts_tags do
            Integer :post_id
            String  :tag
            primary_key [:post_id, :tag]
        end
    end

    down do
        drop_table :posts_tags
    end
end