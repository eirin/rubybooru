Sequel.migration do
  change do
    create_table(:posts_tags) do
      Integer :post_id, :null=>false
      String :tag, :size=>255, :null=>false
      
      primary_key [:post_id, :tag]
    end
    
    create_table(:schema_migrations) do
      String :filename, :size=>255, :null=>false
      
      primary_key [:filename]
    end
  end
end
