require 'rubybooru/api/booru'
require 'rubybooru/api/danbooru'

module RubyBooru::Api
    VALID_SITES = {
        'danbooru'  => {
            :url => 'https://danbooru.donmai.us',
            :class => RubyBooru::Api::Danbooru
        },
        'safebooru' => {
            :url => 'https://safebooru.donmai.us',
            :class => RubyBooru::Api::Danbooru
        }
    }

    def self.get_client name
        begin
            return VALID_SITES[name][:class].new name: name
        rescue NoMethodError
            raise BooruError.new "Invalid site name." \
                "Valid site names: #{VALID_SITES.keys.join(" ")}"
        end
    end
end