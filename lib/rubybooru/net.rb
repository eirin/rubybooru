require 'faraday'
require 'faraday_middleware'
require 'net/http'

module RubyBooru::Net
    class HTTP
        HEADERS = { 'user-agent' => 'RubyBooru/alpha-0' }

        def self.session(site: nil)
            Faraday::Connection.new(site) do |f|
                f.use      StatusCheck
                f.request  :retry, max: 3, interval: 0.05,
                           interval_randomness: 0.5, backoff_factor: 2,
                           exceptions: [UserThrottledError]
                f.response :json, :content_type => /\bjson$/
                #f.response :logger

                f.headers = HEADERS.merge(
                    'content-type' => 'application/json; charset=utf-8'
                )
                
                f.options.params_encoder = DoNotEncode

                f.adapter Faraday.default_adapter
            end
        end

        def self.get url
            $lg.debug("Requesting URL: #{url}")
            Faraday.get(url).body
        end

        def self.stream url
            Faraday::Connection.new(url) do |conn|
                conn.adapter Faraday.default_adapter
                conn.headers = HEADERS

                conn.get(url) do |req|
                    req.options.on_data = Proc.new do |chunk, received|
                        yield(chunk, received)
                    end
                end
            end
        end
    end

    class StatusCheck < Faraday::Response::Middleware
        def on_complete(e)
            if e[:status] == 429
                raise UserThrottledError.new
            elsif ![200, 201, 202, 203, 204].include? e[:status]
                raise BooruNetworkError.new status_code: e[:status]
            end
        end
    end

    class DoNotEncode
        def self.encode(params)
            buffer = ''
            params.each do |key, value|
                buffer << "#{key}=#{value}&"
            end
            return buffer.chop
        end
    end

    STATUS_CODES = {
        200 => {
            :name => 'OK',
            :desc => 'Request was successful'
        }, 204 => {
            :name => 'No Content',
            :desc => 'Request was successful (returned by create actions)'
        }, 400 => {
            :name => 'Bad Request',
            :desc => 'The given parameters could not be parsed'
        }, 401 => {
            :name => 'Unauthorized',
            :desc => 'Authentication failed'
        }, 403 => {
            :name => 'Forbidden',
            :desc => 'Access denied (see help:users for permissions' \
                     'information)'
        }, 404 => {
            :name => 'Not Found',
            :desc => 'Not found'
        }, 410 => {
            :name => 'Gone',
            :desc => 'Pagination limit (see help:users for pagination limits)'
        }, 420 => {
            :name => 'Invalid Record',
            :desc => 'Record could not be saved'
        }, 422 => {
            :name => 'Locked',
            :desc => 'The resource is locked and cannot be modified'
        }, 423 => {
            :name => 'Already Exists',
            :desc => 'Resource already exists'
        }, 424 => {
            :name => 'Invalid Parameters',
            :desc => 'The given parameters were invalid'
        }, 429 => {
            :name => 'User Throttled',
            :desc => 'User is throttled, try again later (see help:users' \
                     'for API limits)'
        }, 500 => {
            :name => 'Internal Server Error',
            :desc => 'A database timeout, or some unknown error occurred on' \
                     'the server'
        }, 502 => {
            :name => 'Bad Gateway',
            :desc => 'Server cannot currently handle the request, try again' \
                     'later (returned during heavy load)'
        }, 503 => {
            :name => 'Service Unavailable',
            :desc => 'Server cannot currently handle the request, try again' \
                     'later (returned during downbooru)'
        }
    }
end