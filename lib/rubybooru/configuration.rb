require 'fileutils'
require 'yaml'

module RubyBooru::Conf
    DEFAULT_CONFIG = {
        'verbosity'     => 'info',
        'log_verbosity' => 'info',
        'store'     => File.join(Dir.home, ".local/share/store"),
        'database'  => {
            'adapter'  => 'sqlite',
            'database' => 'db'
        }
    }

    CONFIG_PATH = File.join(Dir.home, '.config/RubyBooru/config.yml')

    class << self
        def load(config: CONFIG_PATH)
            begin
                $lg.info("Loading configuration from disk: " \
                         "#{File.expand_path(config)}")
                @@configuration = YAML::load(File.open(config))
                @@configuration ||= Hash.new
            rescue Psych::SyntaxError => e
                $lg.error("Syntax error detected in configuration: " \
                          "#{File.expand_path(config)}. " \
                          "Loading default configuration...\n" \
                          "Full error message: #{e.message}")
                @@configuration = DEFAULT_CONFIG.dup
                dump_default config
            rescue Errno::ENOENT
                $lg.error("Configuration file not found: " \
                    "#{File.expand_path(config)}. " \
                    "Loading default configuration...")
                @@configuration = DEFAULT_CONFIG.dup
                dump_default config, override: true
            rescue Exception => e
                $lg.fatal("A fatal error has ocurred. Full error message:")
                puts e.backtrace
                puts e.message
                exit 102
            else
                $lg.debug("Configuration loaded successfully: " \
                                "#{File.expand_path(config)}")
            end

            parse(@@configuration)
        end

        def reload
            load(config: CONFIG_PATH)
            Database.reconnect
            Rec::Record.db = Database.connection
        end

        def configuration
            self.load if !defined?(@@configuration)
            @@configuration
        end

        def dump_default config = CONFIG_PATH, override: false
            config += ".def" if !override
            $lg.info("Writing default configuration to #{config}")
            File.write(config, DEFAULT_CONFIG.to_yaml)
        end

        private
            def parse(conf)
                DEFAULT_CONFIG.each do |k, v|
                    if conf[k].nil?
                        Log.warn("'#{k}' key in configuration is " \
                                        "blank or missing. " \
                                        "Assigning default value to it (#{v}).")
                    end
                    conf[k] ||= v
                end

                begin
                    $lg.log_level  = Logger::Severity.const_get(conf['log_verbosity'].upcase)
                    $lg.sout_level = Logger::Severity.const_get(conf['verbosity'].upcase)
                rescue NameError
                    $lg.error("Wrong verbosity value detected in configuration. " \
                              "Please use one of these: " +
                              Logger::Severity.constants.join(", ").downcase)
                    $lg.warn("Using `info` as default logger level...")
                    $lg.sout_level = Log.log_level = Logger::INFO
                else
                    $lg.debug("Logger level set to #{conf['verbosity']}")
                end

                dir = FileUtils.makedirs(conf['store'])
                %w(media artist_comments comments notes json).each do |d|
                    FileUtils.makedirs(File.join(dir, d))
                end
                $lg.debug("Created store: #{dir}")

                db_config = conf['database']
                if db_config['adapter'] == 'sqlite'
                    db_config['database'] = File.join(conf['store'],
                        db_config['database'])
                    $lg.debug("Database is sqlite, database file " \
                            "location will be: #{db_config['database']}")
                end
            end
    end
end