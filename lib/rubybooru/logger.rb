require 'logger'

module RubyBooru::Log
    class << self
        attr_reader :level, :loggers, :log_level

        def sout_level=(lv)
            @level = lv
            @loggers.first.level = lv
        end

        def log_level=(lv)
            @log_level = lv
            @loggers.last.level = lv
        end

        Logger::Severity.constants.each do |level|
            define_method(level.downcase) do |*args|
                create_loggers if !loggers_created?
                @loggers.each do |logger|
                    logger.send(level.downcase, *args)
                end
            end
        end

        private
            def create_loggers
                @loggers = Array.new

                log_dir = File.join(
                    File.dirname(RubyBooru::Conf::CONFIG_PATH), "log"
                )
                FileUtils.makedirs(log_dir)

                log_path = File.join(log_dir,
                    "log_#{Time.now.strftime("%Y-%m-%d_%H:%M:%S")}.log")

                file_logger = Logger.new(log_path)
                sout_logger = Logger.new(STDOUT)
                @level = file_logger.level = sout_logger.level = Logger::INFO

                FileUtils.ln_sf log_path, File.join(log_dir, "last.log")

                @loggers.append sout_logger
                @loggers.append file_logger
            end
        
            def loggers_created?
                !@loggers.nil? && @loggers.any?
            end
    end
end