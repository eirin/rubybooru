require 'logger'

module RubyBooru
end

require 'rubybooru/configuration'
require 'rubybooru/errors'
require 'rubybooru/logger'

module RubyBooru
    def self.logger;    $lg ||= Log;end
    def self.logger=(l);$lg   = l;  end
end

$lg   = RubyBooru.logger
$conf = RubyBooru::Conf.configuration

require 'rubybooru/api'
require 'rubybooru/database'
require 'rubybooru/net'
require 'rubybooru/query'