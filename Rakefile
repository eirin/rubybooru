# Rakefile

# Required for requiring the project
$:.unshift(File.join(__dir__, "lib"))
require 'rubybooru'

namespace :db do
    desc "Create the database"
    task :create do
        RubyBooru::Database.create
        puts "Database created successfully."
    end

    desc 'Create a database/schema.rb file that is portable against any DB supported by Sequel.'
    task :schema do
        RubyBooru::Database.connect
        RubyBooru::Database.connection.extension :schema_dumper
        dump        = RubyBooru::Database.connection.dump_schema_migration
        schema_path = File.expand_path("../lib/rubybooru/database/schema.rb", __FILE__)
        File.write(schema_path, dump)
        puts "Database schema created at #{schema_path}."
    end

    desc "Drop the database"
    task :drop do
        RubyBooru::Database.drop_database
        puts "Database deleted."
    end

    desc "Reset the database"
    task :reset => [:drop, :create]
end


namespace :g do
    desc "Generate migration"
    task :migration do
        name = ARGV[1] || raise("Specify name: rake g:migration your_migration")
        timestamp = Time.now.strftime("%Y%m%d%H%M%S")
        path = File.expand_path("../database/migrate/#{timestamp}_#{name}.rb", __FILE__)
    
        File.open(path, 'w') do |file|
            file.write <<-EOF
Sequel.migration do
    up do

    end

    down do

    end
end
            EOF
        end
    
        puts "Migration #{path} created"
        abort
    end
end

namespace :debug do
    RubyBooru::Database.connect

    task :shell do
        require 'irb'
        require 'irb/completion'

        ARGV.clear
        #ARGV << ["-I", File.expand_path("./lib")]
        IRB.start
    end
end